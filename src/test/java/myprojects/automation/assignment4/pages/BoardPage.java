package myprojects.automation.assignment4.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class BoardPage {
    private EventFiringWebDriver driver;
    private WebElement boardsElement = null;

    private By boards = By.id("open_board");
    private By createBoard = By.cssSelector("a.board-link.create-board-link");
    private By selectAll = By.cssSelector("span[class='select-all-assets ng-scope']");
    private By nameNewBoard = By.name("boardname");
    private By saveBoard = By.xpath("//a[@class='button' and @type='submit']");

    public BoardPage(EventFiringWebDriver driver) {
        this.driver = driver;
    }

    public void selectBoards() {
        boardsElement = driver.findElement(boards);
        Actions action = new Actions(driver);

        action.moveToElement(boardsElement).build().perform();
        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.visibilityOfElementLocated(createBoard));

        boardsElement.findElement(createBoard).click();
    }

    public void clickSelectAll() {
        driver.findElement(selectAll).click();
    }

    public void fillNewBoardName(String name) {
        driver.findElement(nameNewBoard).sendKeys(name);
    }

    public void clickCreateBoardButton() {
        driver.findElement(saveBoard).click();
    }

    public void clickBoardsButton() {
        //driver.findElement(boards).click();
        boardsElement = driver.findElement(boards);
        Actions action = new Actions(driver);

        action.moveToElement(boardsElement).build().perform();
        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.visibilityOfElementLocated(createBoard));

    }

    public void clickOpenBoardByName(String name) {
        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.visibilityOfElementLocated(createBoard));

        List<WebElement> boards = driver.findElements(By.cssSelector("a[class='board-item-link']")); //a[text()='New Board 1518347392280']
        for(WebElement e: boards) {
            if (e.getText().equals(name)) {
                e.click();
                break;
            }
        }

    }
}
