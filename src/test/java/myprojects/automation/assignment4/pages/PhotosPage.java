package myprojects.automation.assignment4.pages;

import myprojects.automation.assignment4.model.BoardData;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class PhotosPage {
    private EventFiringWebDriver driver;

    private By menuPhotos = By.cssSelector("a[data-nav='nav=nav_Photos']");
    private By buttonActions = By.cssSelector("button.actions-modal-button.ng-scope");
    //private By searchButton = By.cssSelector("figcaption");
    private By searchButton = By.cssSelector("a[class='active']");
    private By productTitle = By.cssSelector("span.checkbox > label");
    private By clearAllPhoto = By.cssSelector("button[class='outline clear-all']");
    private By checkboxPhoto = By.cssSelector("span[class='asset-id ng-binding']");

    public PhotosPage(EventFiringWebDriver driver) {
        this.driver = driver;
    }

    public void clickButtonActions() {
        driver.findElement(buttonActions).click();
    }

    public void clickPhotos() {
        driver.findElement(menuPhotos).click();
    }

    public void clickCopyToBoard(By copyToBoard) {
        driver.findElement(copyToBoard).click();
    }

    public void clickCaptionPhoto() {
        driver.findElement(searchButton).click();
    }

    public void goToBoard(By boardToCopy, BoardData newBoard) {

        List<WebElement> boards = driver.findElements(By.cssSelector("a[ng-bind='board.name']")); //a[text()='New Board 1518347392280']
        for(WebElement e: boards) {
            if (e.getText().equals(newBoard.getName())) {
                e.click();
                break;
            }
        }
    }

    public String clickCheckboxPhoto() {
        driver.findElement(productTitle).click();
        return driver.findElements(checkboxPhoto).get(0).getText();
    }

    public void clickSelectAllPhoto(By selectAll) {
        driver.findElement(selectAll).click();
    }

    public void clickClearAllPhoto() {
        driver.findElement(clearAllPhoto).click();

    }

    public void clickDeleteBoard(By deleteBoard) {
        driver.findElement(deleteBoard).click();

        acceptPopUp();
    }

    public void acceptPopUp() {

        Integer tries = 0;
        Integer maxTries = 30;

        while (tries < maxTries) {
            tries++;

            try {
                disableImplicityWait();
                waitInSeconds(2);
                Alert alert = driver.switchTo().alert();
                if (alert != null && alert.getText().length() > 1) {
                    alert.accept();
                    enableImplicityWait();
                    return;
                }
                waitInSeconds(1);

            } catch (Exception e) {
                e.getSuppressed();
            }
        }
    }

    private void waitInSeconds(int i) {
        try {
            Thread.sleep(i*1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void disableImplicityWait() {
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
    }

    private void enableImplicityWait() {
        driver.manage().timeouts().implicitlyWait(45, TimeUnit.SECONDS);
    }
}

