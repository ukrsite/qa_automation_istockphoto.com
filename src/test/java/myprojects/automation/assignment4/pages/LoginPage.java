package myprojects.automation.assignment4.pages;

import myprojects.automation.assignment4.utils.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;

public class LoginPage {
    private EventFiringWebDriver driver;

    private By signInBtn = By.cssSelector("a.account");
    private By emailInput = By.id("new_session_username");
    private By passwordInput = By.id("new_session_password");
    private By loginBtn = By.id("sign_in");

    public LoginPage(EventFiringWebDriver driver) {
        this.driver = driver;
    }

    public void open() {
        driver.get(Properties.getBaseUrl());
        driver.findElement(signInBtn).click();
    }

    public void fillEmailInput(String login) {
        driver.findElement(emailInput).sendKeys(login);
    }

    public void fillPasswordInput(String password) {
        driver.findElement(passwordInput).sendKeys(password);
    }

    public void clickLoginButton() {
        driver.findElement(loginBtn).click();
    }

    public void clickAccountButton(By collapse) {
        driver.findElement(collapse).click();
    }

    public void close(By signOut) {
        driver.findElement(signOut).click();
    }
}


