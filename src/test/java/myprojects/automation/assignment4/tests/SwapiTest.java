package myprojects.automation.assignment4.tests;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import io.restassured.RestAssured;

import static org.hamcrest.core.IsEqual.equalTo;


public class SwapiTest {

    @Test
    public void shouldGetLukeFromTatooine() {

        JsonPath people = getJsonPath("http://swapi.co/api/people/1/?format=json");
        JsonPath planets = getJsonPath("http://swapi.co/api/planets/1/?format=json");

        Assert.assertTrue(
                "Luke Skywalker".equals(people.getString("name")) ||
                        "Tatooine".equals(planets.getString("name")));
    }

    @Test
    public void shouldGetLuke_C3PO_R2D2() {

        JsonPath people1 = getJsonPath("http://swapi.co/api/people/1/?format=json");
        JsonPath people2 = getJsonPath("http://swapi.co/api/people/2/?format=json");
        JsonPath people3 = getJsonPath("http://swapi.co/api/people/3/?format=json");

        String p1 = people1.getString("name");
        String p2 = people2.getString("name");
        String p3 = people3.getString("name");

        Assert.assertTrue("Luke Skywalker".equals(p1) ||
                        "C-3PO".equals(p2) ||
                        "R2-D2".equals(p3),"");
    }

    public JsonPath getJsonPath(String path) {
        Response response = RestAssured.get(
                path).
                andReturn();
        String json = response.getBody().asString();
        JsonPath jsonPath = new JsonPath(json);
        return jsonPath;

    }

}

