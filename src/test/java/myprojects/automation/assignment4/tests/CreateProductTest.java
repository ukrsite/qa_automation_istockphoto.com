package myprojects.automation.assignment4.tests;

import myprojects.automation.assignment4.BaseTest;
import myprojects.automation.assignment4.model.BoardData;
import myprojects.automation.assignment4.utils.logging.CustomReporter;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CreateProductTest extends BaseTest {

    BoardData boardData = BoardData.generate();

    @DataProvider
    public Object[][] getLoginDate() {
        return new String[][]{{"ukrsite@ukr.net", "Gfhjkm2018"}};
    }

    @Test(dataProvider = "getLoginDate")
    public void loginToBoards(String login, String password) {

        CustomReporter.logAction("Login to boards");
        actions.login(login, password);
    }

    @Test(dependsOnMethods = "loginToBoards")
    public void createNewBoard() {

        CustomReporter.logAction("Create the board");
        actions.createBoard(boardData);

    }

    @Test(dependsOnMethods = "createNewBoard")
    public void copyPhoto() {

        CustomReporter.logAction("Copy the photo");
        actions.copyPhoto(boardData);

    }

    @Test(dependsOnMethods = "copyPhoto")
    public void removePhoto() {

        CustomReporter.logAction("Remove the photo");
        actions.removePhoto();

    }

    @Test(dependsOnMethods = "removePhoto")
    public void removeNewBoard() {

        CustomReporter.logAction("Remove the board");
        actions.removeBoard(boardData);

    }

    @Test(dependsOnMethods = "removeNewBoard")
    public void signOut() {

        CustomReporter.logAction("Sign out");
        actions.signOut();
    }

}
