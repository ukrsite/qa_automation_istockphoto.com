package myprojects.automation.assignment4;

import myprojects.automation.assignment4.model.BoardData;
import myprojects.automation.assignment4.pages.LoginPage;
import myprojects.automation.assignment4.pages.PhotosPage;
import myprojects.automation.assignment4.pages.BoardPage;
import myprojects.automation.assignment4.utils.logging.CustomReporter;
import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

/**
 * Contains main script actions that may be used in scripts.
 */
public class GeneralActions {

    private EventFiringWebDriver driver;
    private WebDriverWait wait;
    private PhotosPage photosPage = null;
    private LoginPage loginPage = null;
    private BoardPage boardPage = null;

    private By account = By.cssSelector("li.wide-header.right-off-canvas-toggle-menu");
    private By inputBoardName = By.name("boardname");
    private By photos = By.cssSelector("a[data-nav='nav=nav_Photos']");
    private By copyToBoard = By.cssSelector("div.option-container > h2");
    private By boardToCopy = By.cssSelector("a.ng-binding");
    private By selectAllPhoto = By.cssSelector("span[class='select-all-assets ng-scope']");
    private By deleteBoard = By.cssSelector("a[class='delete-board ng-scope']");
    private By photoID = By.cssSelector("By.cssSelector('span[class='asset-id ng-binding']')");
    private By inputBoardNamePage = By.cssSelector("div[class='metadata']");

    private By signOut = By.id("hypSignOut");

    public GeneralActions(EventFiringWebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 20);
    }

    /**
     * Logs in to boards.
     *
     * @param login
     * @param password
     */
    public void login(String login, String password) {
        // TODO implement logging in to Boards
        loginPage = new LoginPage(driver);

        loginPage.open();
        loginPage.fillEmailInput(login);
        loginPage.fillPasswordInput(password);
        loginPage.clickLoginButton();
        try {
            waitForContentLoad(account);
        } catch (Exception e) {
            Assert.fail("Login failed");
        }
    }

    public void createBoard(BoardData newBoard) {
        // TODO implement Board creation scenario
        BoardPage boardPage = new BoardPage(driver);

        boardPage.selectBoards();
        waitForContentLoad(inputBoardName);

        boardPage.fillNewBoardName(newBoard.getName());
        waitForContentLoad(account);

        boardPage.clickCreateBoardButton();
        waitForContentLoad(photos);

        boardPage.clickBoardsButton();
        waitForContentLoad(photos);

        boardPage.clickOpenBoardByName(newBoard.getName());
        waitForContentLoad(photos);

        Assert.assertTrue(driver.getTitle().contains(newBoard.getName()), "The new board is not created");
    }

    public void copyPhoto(BoardData newBoard) {
        // TODO implement copy photo scenario
        String photoName;
        photosPage = new PhotosPage(driver);

        photosPage.clickPhotos();
        waitForContentLoad(photos);

        photosPage.clickCaptionPhoto();
        waitForContentLoad(photos);

        photoName = photosPage.clickCheckboxPhoto();
        waitForContentLoad(photos);

        photosPage.clickButtonActions();
        waitForContentLoad(copyToBoard);

        photosPage.clickCopyToBoard(copyToBoard);
        waitForContentLoad(boardToCopy);

        photosPage.goToBoard(boardToCopy, newBoard);
        waitForContentLoad(deleteBoard);

        Assert.assertTrue(((driver.findElements(By.cssSelector("span[class='asset-id ng-binding']"))).size() > 0),
                "Photo not added");
    }
    public void removePhoto() {
        // TODO implement remove photo scenario

        waitForContentLoad(selectAllPhoto);
        photosPage.clickSelectAllPhoto(selectAllPhoto);
        waitForContentLoad(deleteBoard);

        photosPage.clickClearAllPhoto();
        //waitForContentLoad(photos);

//        boardPage.clickBoardsButton();
//        boardPage.clickOpenBoardByName(newBoard.getName());

        driver.navigate().refresh();
        waitForContentLoad(deleteBoard);

        Assert.assertTrue((driver.findElement(By.cssSelector("span.total-asset-count.ng-binding")).getText().isEmpty()),
                "Photo not removed");
    }

    public void removeBoard(BoardData newBoard) {
        // TODO implement remove board scenario
        waitForContentLoad(deleteBoard);
        photosPage.clickDeleteBoard(deleteBoard);
        waitForContentLoad(deleteBoard);

        Assert.assertFalse(driver.findElement(inputBoardNamePage).getText().equals(newBoard.getName()), "The new board is not removed");
    }
    public void signOut() {
        // TODO implement sign out scenario
        loginPage.clickAccountButton(account);
        waitForContentLoad(signOut);
        loginPage.close(signOut);
    }

    /**
     * Waits until page loader disappears from the page
     */
    public void waitForContentLoad(By locator) {
        // TODO implement generic method to wait until page content is loaded
        wait.until(ExpectedConditions.elementToBeClickable(locator));
    }
}
