package myprojects.automation.assignment4.model;

/**
 * Hold Board information that is used among tests.
 */
public class BoardData {
    private String name;

    public BoardData(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    /**
     * @return New Board object with random name.
     */
    public static BoardData generate() {
                return new BoardData(
                "New Board " + System.currentTimeMillis());
    }
}
